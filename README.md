## About

A binding to the [taglib] library.

## Examples

Run `chicken-install -n` to install their dependencies and build them.

## Docs

See [its wiki page].

[taglib]: https://taglib.org/
[its wiki page]: http://wiki.call-cc.org/eggref/5/taglib
